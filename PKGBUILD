# Maintainer: Future Linux Team <future_linux@163.com>

pkgname=tcl
pkgver=8.6.12
pkgrel=1
pkgdesc="Powerful easy-to-learn dynamic programming language"
arch=('x86_64')
url="http://tcl.sourceforge.net/"
license=('custom')
groups=('base')
depends=('glibc' 'zlib')
makedepends=('bash' 'binutils' 'coreutils' 'diffutils' 'gcc' 'grep' 'make' 'sed')
options=('!lto')
source=(https://downloads.sourceforge.net/sourceforge/${pkgname}/${pkgname}${pkgver}-src.tar.gz)
sha256sums=(26c995dd0f167e48b11961d891ee555f680c175f7173ff8cb829f4ebcde4c1a6)

prepare() {
    cd ${pkgname}${pkgver}

    rm -rf pkgs/sqlite3*
}

build() {
    cd ${pkgname}${pkgver}/unix

    SRCDIR=$(pwd)

    ${configure} --mandir=/usr/share/man TCL_LIBRARY=/usr/lib64/tcl8.6

    make

    sed -e "s|$SRCDIR/unix|/usr/lib|" \
        -e "s|$SRCDIR|/usr/include|"  \
        -i tclConfig.sh

    sed -e "s|$SRCDIR/unix/pkgs/tdbc1.1.3|/usr/lib64/tdbc1.1.3|" \
        -e "s|$SRCDIR/pkgs/tdbc1.1.3/generic|/usr/include|"      \
        -e "s|$SRCDIR/pkgs/tdbc1.1.3/library|/usr/lib64/tcl8.6|" \
        -e "s|$SRCDIR/pkgs/tdbc1.1.3|/usr/include|"              \
        -i pkgs/tdbc1.1.3/tdbcConfig.sh

    sed -e "s|$SRCDIR/unix/pkgs/itcl4.2.2|/usr/lib64/itcl4.2.2|" \
        -e "s|$SRCDIR/pkgs/itcl4.2.2/generic|/usr/include|"      \
        -e "s|$SRCDIR/pkgs/itcl4.2.2|/usr/include|"              \
        -i pkgs/itcl4.2.2/itclConfig.sh

    unset SRCDIR
}

package() {
    cd ${pkgname}${pkgver}/unix

    make INSTALL_ROOT=${pkgdir} install

    chmod -v u+w ${pkgdir}/usr/lib64/libtcl8.6.so

    make INSTALL_ROOT=${pkgdir} install-private-headers

    ln -sfv tclsh8.6 ${pkgdir}/usr/bin/tclsh

    mv ${pkgdir}/usr/share/man/man3/{Thread,Tcl_Thread}.3
}
